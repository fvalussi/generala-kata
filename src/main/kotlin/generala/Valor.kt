package generala

inline class Valor(private val valor: Int) {
//    fun sumar(sumando: Int): Valor {
//        return Valor(valor + sumando)
//    }

    fun multiplicar(multiplicando: Int): Valor {
        return Valor(valor * multiplicando)
    }
}
