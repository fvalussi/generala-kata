package generala

class SumarACategoriaDeNumero {
    //    private var multiplicador = Valor(0)
    private var multiplicador = 0

    operator fun invoke(numero: Valor, tirada: List<Valor>): Puntaje {
        recorrerTirada(numero, tirada)
        val puntaje: Valor = numero.multiplicar(multiplicador)
        return Puntaje(puntaje)
    }

    private fun recorrerTirada(numero: Valor, tirada: List<Valor>) {
        tirada.forEach {
            //            if (it == numero) multiplicador.sumar(1)
            if (it == numero) multiplicador = multiplicador + 1
        }
    }
}