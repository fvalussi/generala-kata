package generala

import org.junit.Assert.assertEquals
import org.junit.Test

class SumarACategoriaDeNumeroTest {
    private lateinit var tirada: List<Valor>
    private var puntaje: Puntaje = Puntaje(Valor(0))
    private val sumarACategoriaDeNumero = SumarACategoriaDeNumero()

    @Test
    fun `debe sumar un valor que aparece dos veces`() {
        `dada una tirada de dados con dos 4`()
        `cuando elijo sumar al`(Valor(4))
        `entonces el puntaje es igual a 8`()
    }

    @Test
    fun `debe sumar un valor que aparece tres veces`() {
        `dada una tirada de dados con tres 5`()
        `cuando elijo sumar al`(Valor(5))
        `entonces el puntaje es igual a 15`()
    }

    private fun `dada una tirada de dados con dos 4`() {
        tirada = listOf(Valor(1), Valor(1), Valor(2), Valor(4), Valor(4))
    }

    private fun `cuando elijo sumar al`(numero: Valor) {
        puntaje = sumarACategoriaDeNumero(numero, tirada)
    }

    private fun `entonces el puntaje es igual a 8`() {
        assertEquals(Puntaje(Valor(8)), puntaje)
    }

    private fun `dada una tirada de dados con tres 5`() {
        tirada = listOf(Valor(1), Valor(5), Valor(2), Valor(5), Valor(5))
    }

    private fun `entonces el puntaje es igual a 15`() {
        assertEquals(Puntaje(Valor(15)), puntaje)
    }
}
